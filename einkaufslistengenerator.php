<?php

// Example Call
//http://alexa.itdhosting.de/einkaufslistengenerator.php?user=fabio&command=list&product=salami
//Possible commands: get, list, reset, add&product, remove&product, get

$server   = "127.0.0.1:3306";
$user     = "hhz";
$password = "hhz";
$db       = "einkaufslistengenerator";

$connect = new mysqli($server, $user, $password, $db);
$connect->query('Set NAMES "utf8"');

$user       = $connect->real_escape_string($_GET["user"]);
$command    = $connect->real_escape_string($_GET["command"]);
$product    = $connect->real_escape_string($_GET["product"]);
$product_id = "";

$result           = array();
$result["result"] = "";
$string           = "";

if ($command === "add") {
    $query = "SELECT produkt_id FROM produkt WHERE Bezeichnung = '$product'";
    $sql   = $connect->query($query);
    
    if ($row = $sql->fetch_assoc()) {
        $product_id = $row['produkt_id'];
        $query      = "SELECT * FROM temp_einkaufsliste WHERE kunden_id = 1 AND produkt_id = '$product_id'";
        $sql        = $connect->query($query);
        $row        = $sql->fetch_assoc();
        
        if (!$row['produkt_id']) {
            $query            = "INSERT INTO temp_einkaufsliste(kunden_id, produkt_id) VALUES('1', '$product_id')";
            $sql              = $connect->query($query);
            $result["result"] = "ok";
        } else {
            $result["result"] = "Produkt ist schon in der Liste";
        }
    } else {
        $message = 'Produkt existiert nicht auf der Liste';
        die($message);
    }
}

if ($command === "remove") {
    $query = "SELECT produkt_id FROM produkt WHERE Bezeichnung = '$product'";
    $sql   = $connect->query($query);
    
    if ($row = $sql->fetch_assoc()) {
        $product_id       = $row['produkt_id'];
        $query            = "DELETE FROM temp_einkaufsliste WHERE kunden_id = 1 AND produkt_id = '$product_id'";
        $sql              = $connect->query($query);
        $result["result"] = "ok";
    }
}

if ($command === "reset") {
    $query            = "DELETE FROM temp_einkaufsliste WHERE kunden_id = 1";
    $sql              = $connect->query($query);
    $result["result"] = "ok";
}

if ($command === "list") {
    $query = "SELECT produkt_id FROM temp_einkaufsliste WHERE kunden_id = 1";
    $sql   = $connect->query($query);
    
    while ($row = $sql->fetch_assoc()) {
        $product_id = $row['produkt_id'];
        $query      = "SELECT Bezeichnung FROM produkt WHERE produkt_id = '$product_id'";
        $result2    = $connect->query($query);
        $row2       = $result2->fetch_assoc();
        $string .= $row2['Bezeichnung'] . ', ';
    }
    $result["result"] = $string;
}
print json_encode($result);
$connect->close();
?>